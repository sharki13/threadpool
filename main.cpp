#include <thread>
#include <iostream>
#include <queue>
#include <mutex>
#include <vector>
#include <future>
#include <boost/optional.hpp>
#include <sstream>

std::mutex GI;

void safePrint(const std::string message)
{
    std::unique_lock<std::mutex> lock(GI);
    std::cout << message;
}

template<typename T>
class TaskQueue {
    std::mutex _mtx;
    using unique_lock = std::unique_lock<decltype(_mtx)>;
    std::queue<T> _queue;
    std::condition_variable _popCondition;
    std::condition_variable _pushCondition;
    const unsigned int _maxQueueSize;
public:
    TaskQueue(unsigned int maxQueueSize) : _maxQueueSize(maxQueueSize) {}
    TaskQueue(const TaskQueue&) {};

    void push(const T& task) {
        std::stringstream msg;
        msg << "push Task px=" << &task << std::endl;
        safePrint(msg.str());
        unique_lock lock(_mtx);
        _popCondition.wait(lock, [this]() { return this->_queue.size() < _maxQueueSize;});
        _queue.emplace(task);
        _pushCondition.notify_one();
    }

    T& pop() {
        unique_lock lock(_mtx);
        _pushCondition.wait(lock, [this]() { return !this->_queue.empty();});
        T& ret = std::move(_queue.front());
        std::cout << "Ret Task px=" << &ret << std::endl;
        _queue.pop();
        _popCondition.notify_one();
        return ret;
    }
};

class ThreadPool {
    using Task = std::function<void()>;
    TaskQueue<Task> _taskQueue;
    std::vector<std::thread> _threads;
    unsigned int _threadsNumber;
public:
    ThreadPool(unsigned int threadsNumber, unsigned int queueSize) : _taskQueue(TaskQueue<Task>(queueSize)) {
        for (unsigned int i = 0; i < threadsNumber; ++i)
            _threads.emplace_back([this]() {
            for (;;) {
                auto& task = std::move(this->_taskQueue.pop());
                std::cout << "worker Task px=" << &task << std::endl;
                if (!task)
                    return;
                task();
            }
        });
    }

    ~ThreadPool() {
        for (auto& th : _threads)
            _taskQueue.push(nullptr);
        for (auto& th : _threads)
            th.join();
    }

    template<typename T>
    auto enqueue(T t) -> typename std::enable_if<std::is_same<decltype(t()), void>::value, void>::type
    {
        _taskQueue.push(t);
    }

    template<typename T>
    auto enqueue(T t) -> typename std::enable_if<!std::is_same<decltype(t()), void>::value, std::future<decltype(t())>>::type
    {
        std::cout << "eqnueue px=" << &t << std::endl;
        using return_type = std::result_of<T()>::type;
        using packaged_task = std::packaged_task<return_type()>;
        auto task = std::make_shared<packaged_task>(packaged_task(t));
        _taskQueue.push([task]() {(*task)();});
        return task->get_future();
    }
};

class SomeClass {
public:
    SomeClass() = default;

    int fun(int a, int b) {
        std::cout << "fun executed on thread " << std::this_thread::get_id() << "\n";
        return a + b;
    }
};

int main()
{
    SomeClass someClass;
    auto lambda = []() {std::cout << "Return lambda executed on thread" << std::this_thread::get_id() << " !\n"; return 5;};
    std::cout << "Raw Task px=" << &lambda << std::endl;
    auto lambda2 = []() {std::cout << "NonReturn lambda executed on thread" << std::this_thread::get_id() << " !\n";};

    ThreadPool tp(1, 1);
    tp.enqueue(lambda);
    tp.enqueue(lambda2);
    auto bind = std::bind(&SomeClass::fun, &someClass, 4, 5);
    tp.enqueue(bind);

    system("PAUSE");
}